<?php
/**
 * Options du plugin Lister les dossiers au chargement
 *
 * @plugin     Lister les dossiers
 * @copyright  2014-2017
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Lister_dossiers\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
