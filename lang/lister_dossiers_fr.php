<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

    // C
    'cfg_exemple' => 'Exemple',
    'cfg_exemple_explication' => 'Explication de cet exemple',
    'cfg_titre_parametrages' => 'Paramétrages',

    'dossiers_taille_total' => 'Poids total des répertoires&nbsp;:',

    // I
    'info_dossiers' => 'Dossiers',
    'info_1_dossier' => 'Un dossier',
    'info_nb_dossiers' => '@nb@ dossiers',

    // L
    'lister_dossiers_titre' => 'Lister les dossiers',

    // O

    // P
    'pas_de_dossiers' => 'Il n\'y a pas de dossiers.',

    // S
    'sous_dossiers_afficher' => 'Afficher les sous-dossiers',
    'sous_dossiers_masquer' => 'Masquer les sous-dossiers',

    // T
    'titre_lister_dossiers' => 'Les dossiers',
    'titre_page' => 'Les dossiers',
    'titre_page_configurer_lister_dossiers' => 'Les dossiers, on en fait quoi ?',
);
