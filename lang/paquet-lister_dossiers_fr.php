<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'lister_dossiers_description' => 'Ce plugin va lister les répertoires présents sur votre site au même niveau que SPIP.',
	'lister_dossiers_nom' => 'Lister les dossiers',
	'lister_dossiers_slogan' => 'En un coup d\'oeil, tous vos dossiers.',
);
